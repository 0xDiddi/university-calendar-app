import {Box, createStyles, Paper, RootRef, Theme, Typography, WithStyles} from '@material-ui/core';
import * as React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import {withTheme} from '@material-ui/styles';
import {
    blue,
    indigo,
    green,
    deepPurple,
    pink,
    lightGreen,
    orange, yellow,
} from '@material-ui/core/colors';
import classNames from 'classnames';
import {range} from 'lodash';
import Event from '../Event';
import checkOverlaps from '../overlaps';
import {max, min} from 'date-fns';
import DetailsPopover from './DetailsPopover';

const styles = (theme: Theme) => createStyles({
    size: {
        height: '100%',
        width: '100%',
    },
    text: {
        borderBottom: 'solid 1px black',
        textAlign: 'center',
    },
    primary: {
        backgroundColor: '#333',
    },
    secondary: {
        backgroundColor: '#222',
    },
    h100: {
        height: '100%',
    },
    item: {
        width: '90%',
        marginLeft: '5%',
        marginRight: '5%',
        paddingLeft: 5,
        paddingRight: 5,
    },
});

interface Props extends WithStyles<typeof styles> {
    date: Date;
    primary: boolean;
    contentRef?: (ref: HTMLDivElement) => void;
    isInMonthView?: boolean;
    events: Event[];
    editEvent: (evt: Event) => void;
    deleteEvent: (evt: Event) => void;
    listViewHack?: boolean;
}

interface State {
    popoverOpen: boolean;
    popoverX: number;
    popoverY: number;
    popoverEvent?: Event;
}

interface ItemStyle {
    position: 'absolute' | 'static';
    top?: string;
    transform?: string;
    height?: string;
}

let colours = [yellow, blue, green, orange, deepPurple, lightGreen, indigo, pink];

class DayColumn extends React.Component<Props, State> {
    private min: Date;
    private max: Date;

    constructor(props: Props) {
        super(props);
        let date = props.date;
        this.min = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
        this.max = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);

        this.state = {
            popoverOpen: false,
            popoverX: 0,
            popoverY: 0,
        };
    }

    private filterEvents = (): Event[] => {
        let ret: Event[] = [];

        this.props.events.forEach(evt => {
            if (evt.start >= this.max || evt.end <= this.min) return;
            ret.push(evt);
        });

        return ret;
    };

    private rPos = (pos: number, duration?: number): ItemStyle => {
        if (this.props.isInMonthView) return {position: 'static'};
        pos *= 100 / 24;

        let height: string | undefined = undefined;
        if (duration) height = `${duration / 24 * 100}%`;

        return {
            position: 'absolute',
            top: `${pos}%`,
            height: height,
        }
    };

    private static dateHours = (date: Date): number => {
        return (date.getHours() * 60 + date.getMinutes()) / 60
    };

    private makeEventEntry = (event: Event) => {
        // should never happen, but to narrow the type down to just 'number'...
        if (!event.id) return;

        let colour = colours[event.id % colours.length][700];
        let styleAdjust: any = {
            backgroundColor: colour,
        };
        if (this.props.isInMonthView) styleAdjust['marginTop'] = 5;
        // displayData might still be set, but we don't want it in month view
        else if (event.displayData) {
            let {offset, width} = event.displayData;
            styleAdjust['width'] = `${width * 100 - width * 10}%`;
            styleAdjust['left'] = `${offset * width * 100 - offset * 5}%`;
        }

        let start = max([this.min, event.start]);
        let end = min([this.max, event.end]);

        let itemStyle = this.rPos(DayColumn.dateHours(start), DayColumn.dateHours(end) - DayColumn.dateHours(start));

        return <Paper key={event.id} className={this.props.classes.item}
                      style={{...itemStyle, ...styleAdjust}} onClick={this.openPopover(event)}>
            <Typography>{event.title}</Typography>
        </Paper>
    };

    private openPopover = (event: Event) => (evt: React.MouseEvent) => {
        this.setState({
            popoverOpen: true,
            popoverX: evt.clientX,
            popoverY: evt.clientY,
            popoverEvent: event,
        });
    };

    private closePopover = () => {
        this.setState({popoverOpen: false});
    };

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        let {date, primary, classes} = this.props;
        this.min = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
        this.max = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);

        let dividers: JSX.Element[] = [];
        if (!this.props.isInMonthView)
            dividers = range(1, 24).map(h => {
                let p = this.rPos(h);
                return <div key={h} style={{...p, height: 1, width: '100%', backgroundColor: '#eee'}}/>
            });

        // the ref for the RootRef cannot be undefined, so we pass in a no-op if it isn't defined in props
        const nop = () => {
        };

        let events = this.props.listViewHack ? this.props.events : this.filterEvents();
        if (!this.props.isInMonthView) {
            let overlap = checkOverlaps(events);
            let affectedWidth = 1 / overlap.maxWidth;
            overlap.affected.forEach((e) => {
                e.evt.displayData = {width: affectedWidth, offset: e.offset};
            });
        }

        let evtComponents = events.map(this.makeEventEntry);

        let label = this.props.listViewHack ? undefined :
            <Typography className={classes.text}>{date.getDate() + '.' + (date.getMonth() + 1) + '.'}</Typography>;

        return <Paper className={classNames(classes.size, primary ? classes.primary : classes.secondary)}>
            <DetailsPopover open={this.state.popoverOpen}
                            x={this.state.popoverX} y={this.state.popoverY}
                            event={this.state.popoverEvent} onClose={this.closePopover}
                            editEvent={this.props.editEvent} deleteEvent={this.props.deleteEvent}/>
            <Box display='flex' flexDirection='column' className={classes.h100}>
                {label}

                <RootRef rootRef={this.props.contentRef || nop}>
                    <Box flexGrow={1} position='relative' paddingBottom={this.props.isInMonthView ? '5px' : undefined}>
                        {dividers}
                        {evtComponents}
                    </Box>
                </RootRef>
            </Box>
        </Paper>
    }
}

export default withTheme(withStyles(styles)(DayColumn));
