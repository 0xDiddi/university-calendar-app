import * as React from 'react';
import {
    Button,
    Checkbox, createStyles,
    Dialog,
    DialogActions,
    DialogContent, DialogTitle,
    FormControl,
    FormControlLabel, FormHelperText, IconButton, Input, InputAdornment,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Theme,
    WithStyles,
} from '@material-ui/core';
import {withTheme} from '@material-ui/styles';
import withStyles from '@material-ui/core/styles/withStyles';
import {DatePicker, TimePicker} from '@material-ui/pickers';
import classNames from 'classnames';
import ChipInput from 'material-ui-chip-input';
import Event from '../Event';
import {Clear} from '@material-ui/icons';

const styles = (theme: Theme) => createStyles({
    textField: {
        marginTop: 10,
    },
    // width - 3px, together with the 4px margin on the first three is used to add a small gap between
    // the four fields, yet keep them equally big and spaced
    dateField: {
        marginTop: 10,
        width: 'calc(25% - 3px)',
    },
    dateSpacer: {
        marginRight: 4,
    },
    w50: {
        width: '50%',
    },
    checkbox: {
        paddingLeft: 10,
    },
    hidden: {
        display: 'none',
    },
    error: {
        marginTop: 0,
        marginBottom: 10,
    },
    chipInput: {
        marginBottom: 20,
    },
});

interface Props extends WithStyles<typeof styles> {
    open: boolean,
    onClose: (() => void) | undefined,
    callback: (evt: Event) => void,
    existingEvent?: Event,
}

interface State {
    title?: string,
    location?: string,
    organizer?: string,
    webPage?: string,
    startDate: Date,
    endDate: Date,
    allDay: boolean,
    status: 'Free' | 'Busy' | 'Tentative',
    categories: string[],
    imageData?: string,
    imageFileName?: string,
    faultyFile: boolean,
    validity: Validity,
    lastPreFilledEvent?: Event;
}

interface Validity {
    title: boolean,
    location: boolean,
    organizer: boolean,
    webPage: boolean,
    // validity for dates is enforced by the selectors
    time: boolean,
    messages: string[],
}

class EventDialog extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.resetState(true);
    }

    private resetState = (direct: boolean) => {
        let d = new Date();
        let d1 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
        let d2 = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours() + 1, d.getMinutes());

        let o: State = {
            lastPreFilledEvent: undefined,
            title: undefined,
            location: undefined,
            organizer: undefined,
            webPage: undefined,
            startDate: d1,
            endDate: d2,
            allDay: false,
            status: 'Busy',
            categories: [],
            faultyFile: false,
            validity: {
                title: true,
                location: true,
                organizer: true,
                webPage: true,
                time: true,
                messages: [],
            },
        };

        if (direct) this.state = o;
        else this.setState(o);
    };

    private fileRef: HTMLDivElement | null = null;

    private handleFileSelect = (evt: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>) => {
        const input = (evt.target as HTMLInputElement);
        if (input == null || input.files == null || input.files.length !== 1) return;
        const file = input.files[0];
        const reader = new FileReader();
        let tooLarge = file.size > 500 * 1024;

        this.setState({
            imageFileName: file.name,
            faultyFile: tooLarge,
        });

        if (tooLarge) {
            this.setState({imageData: undefined});
            return;
        }

        reader.onload = (rd: ProgressEvent) => {
            let target = rd.target as FileReader;
            if (target == null || typeof target.result !== 'string') return;
            this.setState({imageData: target.result});
        };
        reader.readAsDataURL(file);
    };

    private handleChange = (name: keyof State) => (event: React.ChangeEvent<HTMLInputElement> |
        React.ChangeEvent<{ name?: string; value: unknown }>) => {
        let newState = {...this.state, [name]: event.target.value};
        newState.validity = this.validate(name, false, newState);
        this.setState(newState);
    };

    private handleChecked = (name: keyof State) => (event: React.ChangeEvent<HTMLInputElement>) => {
        let newState = {...this.state, [name]: event.target.checked};
        newState.validity = this.validate(name, false, newState);
        this.setState(newState);
    };

    // we have to accept null here, as otherwise the date pickers will complain,
    // but as we don't allow them to be cleared, null is not actually an option
    private handleDateChange = (name: 'startDate' | 'endDate', what: 'date' | 'time') => (date: Date | null) => {
        if (date == null) return;
        let d = this.state[name];
        if (what === 'date')
            date.setHours(d.getHours(), d.getMinutes(), 0);
        else if (what === 'time')
            date.setFullYear(d.getFullYear(), d.getMonth(), d.getDate());

        let newState = {...this.state, [name]: date};

        if (name === 'startDate' && date >= this.state.endDate)
            newState['endDate'] = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
                date.getHours() + 1, date.getMinutes());
        else if (name === 'endDate' && date <= this.state.startDate)
            newState['startDate'] = new Date(date.getFullYear(), date.getMonth(), date.getDate(),
                date.getHours() - 1, date.getMinutes());

        newState.validity = this.validate(name, false, newState);
        this.setState(newState);
    };

    private handleCategoryChange = (op: 'add' | 'delete') => (chip: string, index?: number) => {
        let nCat = this.state.categories;
        if (op === 'add') {
            nCat.push(chip);
        } else if (op === 'delete' && index !== undefined) {
            nCat.splice(index, 1);
        }
        this.setState({categories: nCat});
    };

    // having the 'all' parameter here allows us to only check the changed input each time,
    // but have the option to check all when we're about to send stuff
    private validate = (limit: (keyof State) | undefined, all: boolean, s: State): Validity => {
        // we want a copy here
        let v: Validity = JSON.parse(JSON.stringify(s.validity));

        if (limit === 'title' || all) {
            v.messages = v.messages.filter(m => !m.includes('Title'));
            v.title = false;

            if (s.title == null || s.title.length === 0)
                v.messages.push('Title is required.');
            else if (s.title.length > 50)
                v.messages.push('Title mustn\'t be longer than 50 characters.');
            else v.title = true;
        }
        if (limit === 'location' || all) {
            v.messages = v.messages.filter(m => !m.includes('Location'));
            v.location = false;
            if (s.location != null && s.location.length > 50)
                v.messages.push('Location mustn\'t be longer than 50 characters.');
            else v.location = true;
        }
        if (limit === 'organizer' || all) {
            v.messages = v.messages.filter(m => !m.includes('Organizer'));
            v.organizer = false;
            if (s.organizer == null || s.organizer.length === 0)
                v.messages.push('Organizer is required.');
            else if (s.organizer.length > 50)
                v.messages.push('Organizer mustn\'t be longer than 50 characters.');
            else if (!/^[a-zA-Z0-9.-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]+$/.test(s.organizer))
                v.messages.push('Organizer must be a valid email address.');
            else v.organizer = true;
        }
        if (limit === 'webPage' || all) {
            v.messages = v.messages.filter(m => !m.includes('Website'));
            v.webPage = false;
            if (s.webPage != null && s.webPage.length > 100)
                v.messages.push('Website mustn\'t be longer than 100 characters.');
            else v.webPage = true;
        }
        if (limit === 'startDate' || limit === 'endDate' || all) {
            v.messages = v.messages.filter(m => !m.includes('time'));
            v.time = false;
            if (s.startDate > s.endDate)
                v.messages.push('End time must be after start time.');
            else v.time = true;
        }
        return v;
    };

    private send = () => {
        if (!this.state.title || !this.state.organizer) {
            return;
        }

        let e: Event = new Event(new Date(this.state.startDate.getTime()), new Date(this.state.endDate.getTime()), this.state.title, this.state.organizer, this.state.allDay, this.state.status);

        if (this.state.allDay) {
            e.start.setHours(0, 0);
            e.end = new Date(this.state.startDate.getTime());
            e.end.setHours(23, 59);
        }
        if (this.state.location && this.state.location.length > 0) e.location = this.state.location;
        if (this.state.webPage && this.state.webPage.length > 0) e.webpage = this.state.webPage;
        if (this.state.imageData) e.imagedata = this.state.imageData;
        if (this.state.categories.length > 0) e.categories = this.state.categories.map(cat => {
            return {'name': cat}
        });

        if (this.props.existingEvent) {
            e.id = this.props.existingEvent.id;
            // even if no data is present, the name will be set when it was prefilled. If it's empty now,
            // meaning the image was removed by the user, we put the special value that removes the image from the server.
            if (!this.state.imageFileName) e.imagedata = 'REMOVE';
        }

        this.props.callback(e);
    };

    private preFill = () => {
        if (!this.props.existingEvent || this.props.existingEvent === this.state.lastPreFilledEvent) return;
        let evt = this.props.existingEvent;
        // react complains that we don't use setState here.
        // however we are always calling this from within the render method, so we are sure not to miss any updates,
        // and calling setState would re-render the component which results in an infinite loop.
        this.state = {
            lastPreFilledEvent: this.props.existingEvent,
            title: evt.title,
            organizer: evt.organizer,
            webPage: evt.webpage,
            location: evt.location,
            startDate: evt.start,
            endDate: evt.end,
            allDay: evt.allday,
            status: evt.status,
            categories: evt.categories.map(cat => cat.name),
            imageFileName: evt.imagedata,
            faultyFile: false,
            validity: {
                title: true,
                location: true,
                organizer: true,
                webPage: true,
                time: true,
                messages: [],
            },
        };
    };

    private clearImage = (evt: React.MouseEvent) => {
        evt.stopPropagation();
        this.setState({
            imageData: undefined,
            imageFileName: undefined,
            faultyFile: false,
        });
    };

    private onClose = () => {
        if (this.props.onClose)
            this.props.onClose();
        this.resetState(false);
    };

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        this.preFill();
        const {classes} = this.props;
        const fileName = this.state.imageFileName || '';

        const modalTitle = this.props.existingEvent ? 'Edit Event' : 'Add Event';

        const valid = this.state.validity;

        const errors = valid.messages.map(e => <p key={e} className={classes.error}>{e}</p>);

        // instead of checking each condition of state.valid, we can check if any error messages exist.
        // we validate once more here, for all fields, in case they haven't been edited yet.
        const absValid = this.validate(undefined, true, this.state);
        const anyErrors = absValid.messages.length > 0 || this.state.faultyFile;

        // noinspection RequiredAttributes
        return <Dialog open={this.props.open}>
            <DialogTitle>{modalTitle}</DialogTitle>
            <DialogContent>
                {errors}
                <TextField value={this.state.title || ''} label='Title' onChange={this.handleChange('title')} required
                           error={!valid.title} fullWidth/>
                <TextField value={this.state.location || ''} className={classes.textField} label='Location'
                           onChange={this.handleChange('location')}
                           fullWidth/>
                <TextField value={this.state.organizer || ''} className={classes.textField} label='Organizer'
                           helperText='Must be a valid email address'
                           onChange={this.handleChange('organizer')}
                           error={!valid.organizer} required fullWidth/>
                <TextField value={this.state.webPage || ''} className={classes.textField} label='Website'
                           onChange={this.handleChange('webPage')}
                           error={!valid.webPage} fullWidth/>
                <DatePicker format='MMMM do yyyy' label='Start date' autoOk
                            className={classNames(classes.dateField, classes.dateSpacer)} value={this.state.startDate}
                            onChange={this.handleDateChange('startDate', 'date')}
                            views={['year', 'month', 'date']}/>
                <TimePicker ampm={false} label='Start time' autoOk error={!valid.time}
                            className={classNames(classes.dateField, classes.dateSpacer)}
                            value={this.state.startDate} disabled={this.state.allDay}
                            onChange={this.handleDateChange('startDate', 'time')}/>
                <DatePicker format='MMMM do yyyy' label='End date' autoOk
                            className={classNames(classes.dateField, classes.dateSpacer)} value={this.state.endDate}
                            onChange={this.handleDateChange('endDate', 'date')}
                            views={['year', 'month', 'date']} disabled={this.state.allDay}/>
                <TimePicker ampm={false} label='End time' autoOk error={!valid.time}
                            className={classes.dateField}
                            value={this.state.endDate} disabled={this.state.allDay}
                            onChange={this.handleDateChange('endDate', 'time')}/>
                <FormControl className={classNames(classes.textField, classes.w50)}>
                    <InputLabel>Status</InputLabel>
                    <Select value={this.state.status} onChange={this.handleChange('status')}>
                        <MenuItem value={'Free'}>Free</MenuItem>
                        <MenuItem value={'Busy'}>Busy</MenuItem>
                        <MenuItem value={'Tentative'}>Tentative</MenuItem>
                    </Select>
                </FormControl>
                <FormControl className={classNames(classes.textField, classes.w50, classes.checkbox)}>
                    <FormControlLabel checked={this.state.allDay} className={classes.textField}
                                      control={<Checkbox onChange={this.handleChecked('allDay')}/>}
                                      label='All day'/>
                </FormControl>
                <ChipInput value={this.state.categories} fullWidth className={classNames(classes.textField, classes.chipInput)}
                           onAdd={this.handleCategoryChange('add')} helperText='Confirm each category with enter'
                           onDelete={this.handleCategoryChange('delete')}  InputProps={{inputProps: {maxLength: 30}}}/>
                <FormControl error={this.state.faultyFile} fullWidth className={classes.textField}>
                    <InputLabel>Image File</InputLabel>
                    <Input value={fileName} onClick={() => {
                        if (this.fileRef) this.fileRef.click();
                    }}
                           endAdornment={<InputAdornment position='end'>
                               <IconButton onMouseDown={(evt) => evt.preventDefault()}
                                           onClick={this.clearImage}><Clear/></IconButton>
                           </InputAdornment>}/>
                    <FormHelperText>File must not be above 500kB</FormHelperText>
                </FormControl>
                <TextField className={classes.hidden} type='file' inputProps={{accept: 'image/jpeg,image/png'}}
                           onChange={this.handleFileSelect} inputRef={element => this.fileRef = element}/>
            </DialogContent>
            <DialogActions>
                <Button variant='text' onClick={this.onClose}>Cancel</Button>
                <Button variant='outlined' disabled={anyErrors} onClick={this.send}>Send</Button>
            </DialogActions>
        </Dialog>
    }
}

export default withTheme(withStyles(styles)(EventDialog));
