import * as React from 'react';
import {createStyles, Grid, Theme, WithStyles} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import DayColumn from './DayColumn';
import Event from '../Event';

const styles = (theme: Theme) => createStyles({
    h100: {
        height: 'calc(100% - 20px)',
        marginTop: 10,
        marginBottom: 10,
    }
});

interface Props extends WithStyles<typeof styles> {
    events: Event[];
    editEvent: (evt: Event) => void;
    deleteEvent: (evt: Event) => void;
}

interface State {
}

class CalendarViewEventList extends React.Component<Props, State> {
    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        let {classes} = this.props;

        return <Grid container direction='row' className={classes.h100}
                     justify='center' alignItems='stretch'>
            <Grid item xs={12} sm={8} md={4}>
                <DayColumn primary={true} date={new Date()} events={this.props.events} listViewHack={true} isInMonthView={true}
                           editEvent={this.props.editEvent} deleteEvent={this.props.deleteEvent}/>
            </Grid>
        </Grid>;
    }
}

export default withStyles(styles)(CalendarViewEventList);
