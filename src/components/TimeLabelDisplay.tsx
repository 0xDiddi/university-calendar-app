import * as React from 'react';
import {createStyles, Theme, Typography, WithStyles} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import {range} from 'lodash';

const styles = (theme: Theme) => createStyles({
    label: {
        position: 'fixed',
        transform: 'translateY(-50%)',
    },
});

interface Props extends WithStyles<typeof styles> {
    alignPromise: Promise<HTMLDivElement>,
}

interface State {
}

class TimeLabelDisplay extends React.Component<Props, State> {
    private labelRefs: HTMLDivElement[] = [];

    private positionLabels = (test: HTMLDivElement[]) => {
        // the contentRef will be the first element in the result array
        let contentRef = test[0];

        const rect = contentRef.getBoundingClientRect();
        const step = rect.height / 24;

        this.labelRefs.forEach((lbl, i) => {
            let r2 = lbl.getBoundingClientRect();

            lbl.style.top = `${rect.top + step * i}px`;
            lbl.style.left = `${rect.left - r2.width - 10}px`;
        });
    };

    public render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | null {
        let {classes, alignPromise} = this.props;

        let lProm: Promise<void>[] = [];
        let labels = range(25).map(l => {
            let rFunc: (ref: HTMLDivElement) => void;
            lProm[l] = new Promise(resolve => rFunc = (ref: HTMLDivElement) => {
                this.labelRefs[l] = ref;
                resolve();
            });
            // @ts-ignore it is assigned.
            return <Typography key={l} className={classes.label} ref={rFunc}>{l}:00</Typography>;
        });


        // @ts-ignore
        Promise.all([alignPromise, ...lProm]).then(this.positionLabels);

        return <React.Fragment>{labels}</React.Fragment>;
    }
}

export default withStyles(styles)(TimeLabelDisplay);
