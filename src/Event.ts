class Event {

    public id?: number;
    public title: string;
    public location?: string;
    public organizer: string;
    public webpage?: string;
    public start: Date;
    public end: Date;
    public allday: boolean;
    public status: 'Free' | 'Busy' | 'Tentative';
    public categories: { id?: number, name: string }[];
    public imagedata?: string;
    public displayData?: { width: number; offset: number; };

    constructor(startDate: Date, endDate: Date, title: string, organizer: string, allDay: boolean, status: 'Free' | 'Busy' | 'Tentative', location?: string, webPage?: string, categories?: { id?: number, name: string }[], imageData?: string, id?: number) {
        this.start = startDate;
        this.end = endDate;
        this.title = title || '';
        this.location = location;
        this.organizer = organizer || '';
        this.webpage = webPage;
        this.allday = allDay || false;
        this.status = status || 'Busy';
        this.categories = categories || [];
        this.imagedata = imageData;
        this.id = id;
    }
}

export default Event;
